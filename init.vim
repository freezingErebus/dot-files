filetype plugin indent on
call plug#begin(expand('~/.vim/plugged'))
"Plug 'arcticicestudio/nord-vim'" neovim plugin for colorscheme
Plug 'wadackel/vim-dogrun'
Plug 'dracula/vim'
Plug 'sonph/onehalf',{'rtp':'vim/'}
Plug 'joshdick/onedark.vim'
Plug 'arcticicestudio/nord-vim'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'scrooloose/nerdtree'
" devicons for the tree
Plug 'ryanoasis/vim-devicons'
" git plugin
Plug 'tpope/vim-fugitive'
" github colorscheme plugin
Plug 'cormacrelf/vim-colors-github'
" live html and css edit & js evaluation on save
Plug 'turbio/bracey.vim'
" for enabling editorconfig
" for additional info, go to : https://editorconfig.org
Plug 'editorconfig/editorconfig-vim'
Plug 'chrisbra/vim-show-char'
" plugin for git gutter
Plug 'airblade/vim-gitgutter'
" plugin for fuzzy finder
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'airblade/vim-rooter'
" intellisense and syntax highlighting
Plug 'neoclide/coc.nvim', {'branch': 'release' }
Plug 'leafgarland/typescript-vim'
Plug 'peitalin/vim-jsx-typescript'
Plug 'jiangmiao/auto-pairs'
" this is for UNIX-like file editing
Plug 'tpope/vim-eunuch'
" lifesaver
Plug 'vimwiki/vimwiki'
call plug#end()

"" how to use vimwiki
let g:vimwiki_list = [{'path': '~/vimwiki/',
                      \ 'syntax': 'markdown', 'ext': '.md'}]
" <Leader>ww -- Open default wiki index file.
" <Leader>wt -- Open default wiki index file in a new tab.
" <Leader>ws -- Select and open wiki index file.
" <Leader>wd -- Delete wiki file you are in.
" <Leader>wr -- Rename wiki file you are in.
" <Enter> -- Follow/Create wiki link.
" <Shift-Enter> -- Split and follow/create wiki link.
" <Ctrl-Enter> -- Vertical split and follow/create wiki link.
" <Backspace> -- Go back to parent(previous) wiki link.
" <Tab> -- Find next wiki link.
" <Shift-Tab> -- Find previous wiki link.
" :Vimwiki2HTML
" :VimwikiAll2HTML
" :help vimwiki-commands
" :help vimwiki

"coc config
let g:coc_global_extensions = ['coc-emmet', 'coc-css', 'coc-html', 'coc-json', 'coc-prettier', 'coc-tsserver']

set mouse=nicr
set tabstop=4
set shiftwidth=4
set expandtab

" set hybrid-line numbering in vim
set number relativenumber
" don't create swap files or backup or undofiles
set nobackup
set noswapfile
set noundofile
set hidden " switch buffers without saving
set autochdir
set nohlsearch
syntax enable
set backspace=indent,eol,start
" set t_Co=256
set background=dark
if has("termguicolors")
    set termguicolors
endif
set t_Co=256
set t_ut=
colorscheme onehalfdark
" rebind leader key
let mapleader = ","
" integrated terminal plugins
set splitright
set splitbelow
" turn terminal to normal mode with escape
tnoremap <Esc> <C-\><C-n>
" start terminal in insert mode
au BufEnter * if &buftype == 'terminal' | :startinsert | endif
" open terminal on ctrl+n
function! OpenTerminal()
    split term://zsh
    resize 10
endfunction
nnoremap <c-n> :call OpenTerminal()<CR>
autocmd TermOpen * setlocal nonumber norelativenumber


" airline config
let g:airline_powerline_fonts = 1

if !exists('g:airline_symbols')
let g:airline_symbols = {}
endif

" powerline symbols copy-pasted from the airline help page
let g:airline_left_sep = '»'
let g:airline_left_sep = '▶'
let g:airline_right_sep = '«'
let g:airline_right_sep = '◀'
let g:airline_symbols.linenr = '␊'
let g:airline_symbols.linenr = '␤'
let g:airline_symbols.linenr = '¶'
let g:airline_symbols.branch = '⎇'
let g:airline_symbols.paste = 'ρ'
let g:airline_symbols.paste = 'Þ'
let g:airline_symbols.paste = '∥'
let g:airline_symbols.whitespace = 'Ξ'
let g:airline_theme='bubblegum' " github.com/vim-airline/vim-airline/wiki/Screenshots

" tab-line enabled
let g:airline#extensions#tabline#enabled = 1
" i prefer straight tabs over the arrow-like tabs
let g:airline#extensions#tabline#left_sep = ' '
let g:airline#extensions#tabline#left_alt_sep = '|'
" setting formatter amongst: default, jsformatter, unique_tail,
" unique_tail_improved
let g:airline#extensions#tabline#formatter = 'unique_tail'

" nerd-tree config
nnoremap <C-o> :NERDTree<CR>
nnoremap <C-t> :NERDTreeToggle<CR>
nnoremap <C-f> :NERDTreeFind<CR>
" let NERDTreeQuitOnOpen=1
" Start NERDTree when Vim is started without file arguments.
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists('s:std_in') | NERDTree | endif
" Exit Vim if NERDTree is the only window left.
autocmd BufEnter * if tabpagenr('$') == 1 && winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() |
    \ quit | endif
"end of nerd-tree config
" visit https://github.com/preservim/nerdtree for any queries with nerd-tree


" personal key bindings
nnoremap <leader>b :Bracey<CR>
nnoremap <leader>s :BraceyStop<CR>
nnoremap <leader>r :BraceyReload<CR>
nnoremap <leader>p :FZF<CR>
nnoremap <S-k> gt
nnoremap <S-j> gT
nnoremap <S-n> :tabnew<CR>
nnoremap <leader>f :Rg<CR>

" config for whitespaces
" fyi : shortcut for the whitespace toggle command is <leader>ws
let g:showwhite_space_char = "•"

" disabling normal arrow key-bindings
nnoremap <Up> <Nop>
nnoremap <Down> <Nop>
nnoremap <Left> <Nop>
nnoremap <Right> <Nop>

" config for gitgutter inspired from sublime
nnoremap <leader>gg :GitGutterToggle<CR